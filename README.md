# CertPull

Python script to obtain issued-to SSL certificate information. 

```
xxxx :: xxxx/CertPull » ./certpull.py -f /xxxxx/ips.txt
[+]Attempting to retrieve SSL certification information -  17Mar19

138.197.53.111  -  [('DNS', 'kirstengillibrand.com'), ('DNS', 'www.kirstengillibrand.com')]
103.73.66.132  -  [('DNS', 'www.zvhgroup.ru'), ('DNS', 'zvhgroup.ru')]
95.215.19.10  -  [('DNS', 'njal.la'), ('DNS', 'www.njal.la')]
209.15.20.231  -  [('DNS', '*.pressable.com'), ('DNS', 'pressable.com')]
```

```
xxxx :: xxxx/CertPull » ./certpull.py -h
usage: certpull.py [-h] [-i IP] [-p PORT] [-t TIMEOUT] [-f FILE]

optional arguments:
  -h, --help            show this help message and exit
  -i IP, --ip IP        IP to test against (target ip)
  -p PORT, --port PORT  SSL Port. Default: 443
  -t TIMEOUT, --timeout TIMEOUT
                        Specify timeout for connection. Default: 5 seconds
  -f FILE, --file FILE  File containing a list of valid IPs. Doesn't support
                        CIDR notation.

```