#!/usr/bin/env python3

# Script is being developed. Expect bugs and unnecessary imports.
import socket
import eventlet
import sys
import datetime
import argparse
from requests.packages.urllib3.contrib import pyopenssl as reqs




def pullcert(host, port,connTimeout):
	try:
		with eventlet.Timeout(connTimeout):
		    x509 = reqs.OpenSSL.crypto.load_certificate(
		        reqs.OpenSSL.crypto.FILETYPE_PEM,
		        reqs.ssl.get_server_certificate((host, port))
		    )
		    data = reqs.get_subj_alt_name(x509)
		    return data
	except eventlet.timeout.Timeout:
		return "[+]ERROR: connection times out."
	except socket.error:
		return "[+]ERROR: Could not connect to target."

if __name__ == '__main__':
	eventlet.monkey_patch()
	x = datetime.datetime.now()
	tDate = x.strftime("%d%b%y")
	parser = argparse.ArgumentParser()
	parser.add_argument("-i", "--ip", required=False, help="IP to test against (target ip)")
	parser.add_argument("-p", "--port", default=443, required=False, type=int, help="SSL Port. Default: 443")
	parser.add_argument("-t", "--timeout", default=5, required=False, type=int, help="Specify timeout for connection. Default: 5 seconds")
	parser.add_argument("-f", "--file", type=argparse.FileType('r'), help="File containing a list of valid IPs. Doesn't support CIDR notation.")
	args = parser.parse_args()

	print("[+]Attempting to retrieve SSL certification information - ", tDate, "\n")
	connTimeout = args.timeout
	tIP = args.ip
	tPort = args.port
	if args.file:
		for i in args.file:
			i = i.rstrip()
			domains = pullcert(i,tPort,connTimeout)
			print(i," - ",domains)
		exit()
	else:
		pass
	domains = pullcert(tIP,tPort,connTimeout)
	print(tIP," - ",domains)